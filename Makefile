serve:
	hugo server --disableFastRender -w

deploy:
	hugo
	rm -r docs/csv
	sh deployrsync.sh
