---
title: 'Composiciones'
date: 2019-03-10
layout: 'composiciones'
heroHeading: 'Composiciones'
heroSubHeading: ""
heroBackground: 'images/clarin.png'
---

# Nuevas Tonadas VI

Colaboración en edición nº VI de **Nuevas Tonadas**, publicación de la *Banda de Gaitas de Boto Aragonesas*.



