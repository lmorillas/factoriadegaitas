---
title: 'Madera'
date: 2018-11-18T12:33:46+10:00
icon: 'images/maderai.png'
draft: false
featured: true
weight: 1
heroHeading: 'Madera'
heroSubHeading: 'We offer general accouting on hourly rate or fixed fee'
heroBackground: 'images/madera.jpg'
---

La madera utilizada es el BOJ DEL PIRINEO. El boj está protegido y se vende mediante permisos (trabajamos únicamente con madera cortada legalmente).

Los adornos, que aportan ese toque estético tan interesante a las gaitas de La Factoría, están realizados en GRANADILLO (la madera más densa que hay), NAZARENO y PALO SANTO.

La gran calidad de estos materiales y el contraste estético resultante del abrazo entre ellos, hacen de estas gaitas instrumentos únicos.
