---
title: "Mis Gaitas"
date: 2019-03-27T11:08:26+01:00
heroHeading: 'Mis Gaitas'
heroSubHeading: 'Desarrollar la gaita de boto'
heroBackground: 'images/tornohb.jpg'
layout: 'aboutlayout'
---
Hace unos años puse en marcha en Arguis junto a mi padre el proyecto
**La factoría de gaitas**, *fruto de la necesidad e interés por investigar y desarrollar la gaita de boto*. 

